package com.gooddata.feedback.api;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.gooddata.feedback.api.dto.FeedbackCreateDto;
import com.gooddata.feedback.api.dto.FeedbackDto;
import com.gooddata.feedback.mediator.FeedbackMediator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Resource for storing and retrieving of feedback
 */
@RestController
public final class FeedbackResource {

    private FeedbackMediator feedbackMediator;

    @Autowired
    public FeedbackResource(FeedbackMediator feedbackMediator) {
        this.feedbackMediator = feedbackMediator;
    }

    /**
     * Method for storing new feedback
     * @param feedbackCreateDto Object with feedback information
     * @return Stored feedback
     */
    @RequestMapping(path = "/", method = POST)
    public FeedbackDto add(@Valid @RequestBody FeedbackCreateDto feedbackCreateDto) {
        return feedbackMediator.add(feedbackCreateDto);
    }

    /**
     * Method for retrieving of all stored feedback
     * @return List of all stored feedback
     */
    @RequestMapping(path = "/", method = GET)
    public List<FeedbackDto> getAll() {
        return feedbackMediator.getAll();
    }

    /**
     * Method for retrieving of all feedback of specified person
     * @param name Name of person who stored feedback
     * @return List of all stored feedback of specified person
     */
    @RequestMapping(path = "/filter", method = GET)
    public List<FeedbackDto> getByName(@RequestParam(value = "name") String name) {
        return feedbackMediator.getByName(name);
    }

}
