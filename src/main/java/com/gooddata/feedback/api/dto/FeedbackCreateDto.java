package com.gooddata.feedback.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Object for storing information about new feedback.
 */
@Data
public class FeedbackCreateDto {

    @NotBlank
    @JsonProperty("name")
    private String name;

    @NotBlank
    @JsonProperty("message")
    private String message;

}
