package com.gooddata.feedback.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Object containing information about feedback.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FeedbackDto extends FeedbackCreateDto {

    @NotNull
    @JsonProperty("id")
    private UUID id;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("created")
    private LocalDateTime created;

}
