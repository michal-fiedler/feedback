package com.gooddata.feedback.db.dal;

import com.gooddata.feedback.db.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, UUID> {

    Feedback save(Feedback feedback);
    List<Feedback> findAll();
    List<Feedback> findByName(String name);

}
