package com.gooddata.feedback.mediator.mapper;

import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class OrikaBeanMapper extends ConfigurableMapper {
}
