package com.gooddata.feedback.mediator;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;

import com.gooddata.feedback.api.dto.FeedbackCreateDto;
import com.gooddata.feedback.api.dto.FeedbackDto;
import com.gooddata.feedback.db.dal.FeedbackRepository;
import com.gooddata.feedback.db.model.Feedback;
import com.gooddata.feedback.mediator.mapper.OrikaBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

@Component
public class FeedbackMediator {

    private FeedbackRepository feedbackRepository;
    private OrikaBeanMapper orikaBeanMapper;

    @Autowired
    public FeedbackMediator(
            FeedbackRepository feedbackRepository,
            OrikaBeanMapper orikaBeanMapper) {
        this.feedbackRepository = feedbackRepository;
        this.orikaBeanMapper = orikaBeanMapper;
    }

    @Transactional
    public FeedbackDto add(FeedbackCreateDto feedbackCreateDto) {
        Feedback feedback = orikaBeanMapper.map(feedbackCreateDto, Feedback.class);
        feedback.setId(UUID.randomUUID());
        feedback.setCreated(now());
        Feedback addedFeedback = feedbackRepository.save(feedback);
        return orikaBeanMapper.map(addedFeedback, FeedbackDto.class);
    }

    public List<FeedbackDto> getAll() {
        List<Feedback> feedbackList = feedbackRepository.findAll();
        return mapList(feedbackList);
    }

    public List<FeedbackDto> getByName(String name) {
        List<Feedback> feedbackList = feedbackRepository.findByName(name);
        return mapList(feedbackList);
    }

    private List<FeedbackDto> mapList(List<Feedback> feedbackList) {
        return feedbackList.stream().map(f -> orikaBeanMapper.map(f, FeedbackDto.class)).collect(toList());
    }
}
