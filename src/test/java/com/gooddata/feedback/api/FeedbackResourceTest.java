package com.gooddata.feedback.api;

import static java.time.LocalDateTime.now;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import com.gooddata.feedback.api.dto.FeedbackCreateDto;
import com.gooddata.feedback.api.dto.FeedbackDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Integration test for Feedback resource.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class FeedbackResourceTest {

    private static final String NAME1 = "Chuck Norris";
    private static final String NAME2 = "John Doe";
    private static final String MESSAGE = "This is test feedback.";
    private static final int NUMBER_OF_REQUIRED_FEEDBACK = 1;
    private static final int NUMBER_OF_FEEDBACK = 4;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private FeedbackResource feedbackResource;

    @Before
    public void setUp() {
        jdbcTemplate.update("DELETE FROM feedback");
    }

    @Test
    public void add() {
        FeedbackDto feedbackDto = feedbackResource.add(createFeedbackCreateDto());

        assertThat(feedbackDto.getId(), is(any(UUID.class)));
        assertThat(feedbackDto.getCreated(), is(any(LocalDateTime.class)));
        assertThat(feedbackDto.getName(), is(NAME1));
        assertThat(feedbackDto.getMessage(), is(MESSAGE));
        assertThat(jdbcTemplate.queryForObject("SELECT COUNT(*) FROM feedback", Integer.class),
                is(NUMBER_OF_REQUIRED_FEEDBACK));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void addWithoutName() {
        FeedbackCreateDto feedbackCreateDto = createFeedbackCreateDto();
        feedbackCreateDto.setName(null);

        feedbackResource.add(feedbackCreateDto);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void addWithoutMessage() {
        FeedbackCreateDto feedbackCreateDto = createFeedbackCreateDto();
        feedbackCreateDto.setMessage(null);

        feedbackResource.add(feedbackCreateDto);
    }

    @Test
    public void getAll() {
        for (int i = 0; i < NUMBER_OF_FEEDBACK; i++) {
            InsertFeedbackToDb(NAME1, MESSAGE);
        }

        List<FeedbackDto> feedbackDtos = feedbackResource.getAll();
        assertThat(feedbackDtos.size(), is(NUMBER_OF_FEEDBACK));
    }

    @Test
    public void getByName() {
        for (int i = 0; i < NUMBER_OF_FEEDBACK; i++) {
            InsertFeedbackToDb(NAME1, MESSAGE);
        }
        InsertFeedbackToDb(NAME2, MESSAGE);

        List<FeedbackDto> feedbackDtos = feedbackResource.getByName(NAME2);
        assertThat(feedbackDtos.size(), is(NUMBER_OF_REQUIRED_FEEDBACK));
    }

    private void InsertFeedbackToDb(String name, String message) {
        jdbcTemplate.update("INSERT INTO feedback (id, created, name, message) VALUES(?, ?, ?, ?)",
                UUID.randomUUID(), now(), name, message);
    }

    private static FeedbackCreateDto createFeedbackCreateDto() {
        FeedbackCreateDto feedbackCreateDto = new FeedbackCreateDto();
        feedbackCreateDto.setName(NAME1);
        feedbackCreateDto.setMessage(MESSAGE);

        return feedbackCreateDto;
    }

}