package com.gooddata.feedback.mediator;

import static java.time.LocalDateTime.now;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.gooddata.feedback.api.dto.FeedbackCreateDto;
import com.gooddata.feedback.api.dto.FeedbackDto;
import com.gooddata.feedback.db.dal.FeedbackRepository;
import com.gooddata.feedback.db.model.Feedback;
import com.gooddata.feedback.mediator.mapper.OrikaBeanMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class FeedbackMediatorTest {

    private static final UUID ID = UUID.randomUUID();
    private static final LocalDateTime CREATED = now();
    private static final String NAME = "Chuck Norris";
    private static final String MESSAGE = "This is test feedback.";

    @Mock
    private FeedbackRepository feedbackRepository;

    private FeedbackMediator feedbackMediator;

    @Before
    public void setUp() {
        OrikaBeanMapper orikaBeanMapper = new OrikaBeanMapper();
        feedbackMediator = new FeedbackMediator(feedbackRepository, orikaBeanMapper);
    }

    @Test
    public void add() {
        when(feedbackRepository.save((Feedback) any())).thenReturn(createFeedback());

        FeedbackDto feedbackDto = feedbackMediator.add(createFeedbackCreateDto());
        assertThat(feedbackDto.getId(), is(Matchers.any(UUID.class)));
        assertThat(feedbackDto.getCreated(), is(Matchers.any(LocalDateTime.class)));
        assertThat(feedbackDto.getName(), is(NAME));
        assertThat(feedbackDto.getMessage(), is(MESSAGE));
    }

    @Ignore
    @Test
    public void getAll() {
        // TODO
    }

    @Ignore
    @Test
    public void getByName() {
        // TODO
    }

    private static FeedbackCreateDto createFeedbackCreateDto() {
        FeedbackCreateDto feedbackCreateDto = new FeedbackCreateDto();
        feedbackCreateDto.setName(NAME);
        feedbackCreateDto.setMessage(MESSAGE);

        return feedbackCreateDto;
    }

    private static Feedback createFeedback() {
        Feedback feedback = new Feedback();
        feedback.setId(ID);
        feedback.setCreated(CREATED);
        feedback.setName(NAME);
        feedback.setMessage(MESSAGE);

        return feedback;
    }

}